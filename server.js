"use strict";
var PORT		= 3000;
var express		= require('express');
var path		= require('path');
var cookie		= require('cookie-parser');
var bodyParser	= require('body-parser');
var less		= require('less-middleware');
var session		= require('express-session');
var routes		= require('./routes/index');
var multer		= require('multer');
var app			= express();
var mysql = require('mysql');
var crypto  = require('crypto');
var pool = mysql.createPool({
  host    : "localhost",
  user    : "username",
  password  : "password",
  database  : "database"
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(less(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use(multer({ dest: './uploads'}));
app.engine('html', require('ejs').renderFile);
app.use('/', routes);

app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error.html', {
		message: err.message
	});
});

var http = require('http').Server(app);
var io = require('socket.io')(http);

io.on('connection', function(socket){


// function for venue
  socket.on('venue_connection', function(data){

    pool.getConnection(function(error, db) {
      if (error) {
        console.log(error);
      }

      // update chat status       
      db.query("UPDATE venue_data SET chat_status = 1 WHERE venue_id = ?",
            [data.venue_id],
            function(error) {
              if (error) {   
                 console.log(error);        
              }
            });

      // member online
      db.query("SELECT member_id , first_name , last_name , (select count(chat_log.chat_id) from chat_log where member_data.member_id = chat_log.chat_from and user_type = 'member' and read_status = 0) as total_log FROM `member_data` where chat_status = 1",
      function(error, records) {
        if (error == null) {
                io.emit('member_online', records);
              //  console.log('user online ('+records.length+')');
        } else {
          console.log(error);
        }
      });

      // member offline
      db.query("SELECT member_id , first_name , last_name , (select count(chat_log.chat_id) from chat_log where member_data.member_id = chat_log.chat_from and user_type = 'member' and read_status = 0) as total_log FROM `member_data` where chat_status = 0",
      function(error, records) {
        if (error == null) {
                io.emit('member_offline', records);
              //  console.log('user offline ('+records.length+')');
        } else {
          console.log(error);
        }
      });      

      db.release();

    });


  });

  // login functions
  socket.on('login', function(data){
      var digest = crypto.createHash("md5").update(data.password)
    .digest("hex");

    pool.getConnection(function(error, db) {
    if (error) {
      console.log(error);
      return;
    }

      db.query("select venue_id,token,venue_name,venue_user from venue_data where venue_user=? and venue_pass=?",
      [data.email,digest],
      function(error, records) {
        if (error == null) {
            // random new token
            var token = crypto.createHash("md5").update(new Date().toISOString()).digest('hex');
            
            // update token           
            db.query("UPDATE venue_data SET token = ? WHERE venue_id = ?",
            [token,records[0].venue_id],
            function(error) {
              if (error == null) {

                socket.emit('updatechat', {
                  venue_id: records[0].venue_id,
                  venue_name: records[0].venue_name,
                  venue_user: records[0].venue_user,
                  token: token
                });
              }
            });


        }


      });
      db.release();
  });

  });

  // venue chat to member
  socket.on('venue_chat', function(data){
  	var room =  data.venue + 'to' + data.member;

      pool.getConnection(function(error, db) {
      if (error) {
      console.log(error);
      return;
      }

        db.query("INSERT INTO chat_log (chat_from,message,chat_to,chat_room,read_status,user_type) VALUES (?,?,?,?,?,'venue')",
        [data.venue,data.message,data.member,room,data.read_status],
        function(error, records) {
          if (error == null) {

            io.emit('venue_chat', room, data.message);
            console.log('In Room '+ room +' ' + data.from + ' : ' + data.message);        

          } else {
          console.log(error);
          }

        });
        db.release();
      });
  });  

  // when switch room
  socket.on('venue_switch', function(data){
    var member_room =  data.member + 'to' + data.venue;
    var venue_room =  data.venue + 'to' + data.member;

    // update old room
    io.emit('server', data.old_room , 0);

    // update new room
    io.emit('server', member_room , 1);
 
    pool.getConnection(function(error, db) {
      if (error) {
        console.log(error);
      }

      // update chat status       
      db.query("SELECT * FROM chat_log where chat_room = ? or chat_room = ?",
            [member_room,venue_room],
            function(error ,records) {
              if (error == null) {              

                    io.emit('history_log', records , member_room, venue_room);

                // update chat status       
                db.query("UPDATE chat_log SET read_status = 1 WHERE chat_to = ? and chat_room = ?",
                [data.venue,member_room],
                function(error) {
                    if (error == null) {              
                      console.log('update read status');
                    }
                  });  

                // member online
                db.query("SELECT member_id , first_name , last_name , (select count(chat_log.chat_id) from chat_log where member_data.member_id = chat_log.chat_from and user_type = 'member' and read_status = 0) as total_log FROM `member_data` where chat_status = 1",
                function(error, records) {
                    if (error == null) {
                        io.emit('member_online', records);
                    } else {
                        console.log(error);
                    }
                });

                // member offline
                db.query("SELECT member_id , first_name , last_name , (select count(chat_log.chat_id) from chat_log where member_data.member_id = chat_log.chat_from and user_type = 'member' and read_status = 0) as total_log FROM `member_data` where chat_status = 0",
                function(error, records) {
                    if (error == null) {
                        io.emit('member_offline', records);
                    } else {
                        console.log(error);
                    }
                });

              } else {
                console.log(error);
              }
            });     
        db.release();
    });
  
  });  

  // when user disconnect
  socket.on('venue_disconnect', function(data){

    pool.getConnection(function(error, db) {
      if (error) {
        console.log(error);
      }

      // update chat status       
      db.query("UPDATE venue_data SET chat_status = 0 WHERE venue_id = ?",
            [data.venue_id],
            function(error) {
              if (error == null) {              
                io.emit('server', data.old_room , 0);
                console.log(data.old_room);
                console.log(data.venue_name + ' has offline.');
              }
            });     
      db.release();
    });
  });


// function for member
  // when member connection
  socket.on('member_connection', function(data){
    var member_room =  data.member + 'to' + data.venue;
    var venue_room =  data.venue + 'to' + data.member;
    pool.getConnection(function(error, db) {
      if (error) {
        console.log(error);
      }

      // update chat status       
      db.query("UPDATE member_data SET chat_status = 1 WHERE member_id = ?",
      [data.member],
          function(error) {
              if (error) {   
                 console.log(error);        
              } else {
                io.emit('server', venue_room , 1);
              }
          });

      // update chat status       
      db.query("SELECT * FROM chat_log where chat_room = ? or chat_room = ?",
      [member_room,venue_room],
          function(error ,records) {
              if (error == null) {             
                  io.emit('history_log', records , member_room, venue_room);
          }
      });

      // member online
      db.query("SELECT member_id , first_name , last_name , (select count(chat_log.chat_id) from chat_log where member_data.member_id = chat_log.chat_from and user_type = 'member' and read_status = 0) as total_log FROM `member_data` where chat_status = 1",
      function(error, records) {
        if (error == null) {
                io.emit('member_online', records);
        } else {
          console.log(error);
        }
      });

      // member offline
      db.query("SELECT member_id , first_name , last_name , (select count(chat_log.chat_id) from chat_log where member_data.member_id = chat_log.chat_from and user_type = 'member' and read_status = 0) as total_log FROM `member_data` where chat_status = 0",
      function(error, records) {
        if (error == null) {
                io.emit('member_offline', records);
        } else {
          console.log(error);
        }
      });      

      db.release();

    });

  });

  // venue chat to member
  socket.on('member_chat', function(data){

    var room =  data.member + 'to' + data.venue;

        pool.getConnection(function(error, db) {
          if (error) {
            console.log(error);
          return;
          }

              db.query("INSERT INTO chat_log (chat_from,message,chat_to,chat_room,read_status,user_type) VALUES (?,?,?,?,?,'member')",
              [data.member,data.message,data.venue,room,data.read_status],
                  function(error, records) {
                      if (error == null) {

                          io.emit('member_chat', room, data.message);
                          console.log('In Room '+ room +' ' + data.from + ' : ' + data.message);    

                          // member online
                          db.query("SELECT member_id , first_name , last_name , (select count(chat_log.chat_id) from chat_log where member_data.member_id = chat_log.chat_from and user_type = 'member' and read_status = 0) as total_log FROM `member_data` where chat_status = 1",
                          function(error, records) {
                              if (error == null) {
                                  io.emit('member_online', records);
                              } else {
                                  console.log(error);
                              }
                          });

                          // member offline
                          db.query("SELECT member_id , first_name , last_name , (select count(chat_log.chat_id) from chat_log where member_data.member_id = chat_log.chat_from and user_type = 'member' and read_status = 0) as total_log FROM `member_data` where chat_status = 0",
                          function(error, records) {
                          if (error == null) {
                              io.emit('member_offline', records);
                          } else {
                              console.log(error);
                          }
                          });    

                      } else {
                          console.log(error);
                      }            

              });
        db.release();
      });
  });   

  // when member disconnect
  socket.on('member_disconnect', function(data){
      var member_room =  data.member_id + 'to' + data.venue;
      var venue_room =  data.venue + 'to' + data.member_id;
    pool.getConnection(function(error, db) {
      if (error) {
        console.log(error);
      }

      // update chat status       
      db.query("UPDATE member_data SET chat_status = 0 WHERE member_id = ?",
            [data.member_id],
            function(error) {
              if (error == null) {    

              // member online
              db.query("SELECT member_id , first_name , last_name , (select count(chat_log.chat_id) from chat_log where member_data.member_id = chat_log.chat_from and user_type = 'member' and read_status = 0) as total_log FROM `member_data` where chat_status = 1",
                  function(error, records) {
                    if (error == null) {
                      io.emit('member_online', records);
                    } else {
                      console.log(error);
                    }
              });

              // member offline
             db.query("SELECT member_id , first_name , last_name , (select count(chat_log.chat_id) from chat_log where member_data.member_id = chat_log.chat_from and user_type = 'member' and read_status = 0) as total_log FROM `member_data` where chat_status = 0",
               function(error, records) {
                if (error == null) {
                  io.emit('member_offline', records);
                } else {
                  console.log(error);
                }
              });
             io.emit('server', venue_room , 0);
              console.log(data.member_name + ' has offline.');
              }
            });     
        db.release();
    });
  });   

});


var server = http.listen(PORT, function() {
	console.log('Server port ' + server.address().port);
});

/*
var fs = require('fs');
var https = require('https');
var options = {
	key:  fs.readFileSync('/path/url.key'),
	cert: fs.readFileSync('/path/url.crt')
};

var sslServer = https.createServer(options, app).listen(PORT + 1, function(){
	console.log("SSL port " + (PORT + 1));
});
*/
