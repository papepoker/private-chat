Private Chat (Chat for booking a restaurant) By Sirirat Saemak.
==============
By Node.js / Express / EJS / Socket.IO Version 1.2.1 / MySQL / Bootstrap.

Installation
------------
```shell
    $ npm install
    $ node server
```

member chat room
==============
http://127.0.0.1:4500/member/chat/:id/:token

Example URL (member chat room)
-------
http://127.0.0.1:4500/member/chat/10/73a15754787d58c20eb21089097aa2aa

venue sign-in
==============
http://127.0.0.1:4500
