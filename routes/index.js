"use strict";
var express	= require("express");
var router	= express.Router();
var mysql	= require('mysql');
var crypto	= require('crypto');
var pool = mysql.createPool({
	host		: "localhost",
  	user    : "username",
  	password  : "password",
  	database  : "database"
});
// default page
router.get("/", function(req, res) {
		//res.render("index.html");
		// redirect to venues login
		 res.redirect("/venues/signin");

});

// venues models
router.get("/venues/signin", function(req, res) {
	res.render("venues/signin.html");
});

// venue chat
router.get("/venues/chat/:venue_id/:token", function(req, res) {

	if (req.params == null) {
		res.redirect("/venues/signin");		
	}

	pool.getConnection(function(error, db) {
		if (error) {
			res.redirect("/");
			return;
		}

			db.query("select venue_id,venue_name,token from venue_data where venue_id =? and token = ?", 
			[req.params.venue_id,req.params.token], function(error, records) {
			if (!error) {
					res.render("venues/chat.html", { token: records[0].token,
													venue_name: records[0].venue_name,
													venue_id: req.params.venue_id});	
					console.log('Hi '+ records[0].venue_name);
				} else {
					console.log('Please Login !!');
					res.redirect("/venues/signin");
				}

		});
		db.release();
	});

});

router.get("/member/signin", function(req, res) {
	res.render("member/signin.html");
});

// member chat
router.get("/member/chat/:member_id/:token", function(req, res) {

	if (req.params == null) {
		res.redirect("/member/signin");
	}

	pool.getConnection(function(error, db) {
		if (error) {
			res.redirect("/member/signin");
			return;
		}

			db.query("select member_id,first_name,token from member_data where member_id =? and token = ?", 
			[req.params.member_id,req.params.token], function(error, records) {
			if (!error) {
					res.render("member/chat.html", { token: records[0].token,
													member_id: records[0].member_id,
													member_name: records[0].first_name});	
					console.log('Hi '+ records[0].first_name);
				} else {
					console.log('Please Login !!');
					res.redirect("/member/signin");
				}
		});
		db.release();	
	});

});
module.exports = router;
