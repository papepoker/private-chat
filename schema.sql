-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Table structure for table `chat_log`
--

CREATE TABLE IF NOT EXISTS `chat_log` (
  `chat_id` int(11) NOT NULL AUTO_INCREMENT,
  `chat_from` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `chat_to` int(11) NOT NULL,
  `chat_room` varchar(255) NOT NULL,
  `read_status` int(11) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  PRIMARY KEY (`chat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;


-- --------------------------------------------------------

--
-- Table structure for table `member_data`
--

CREATE TABLE IF NOT EXISTS `member_data` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `e_mail` varchar(255) NOT NULL,
  `mem_password` varchar(255) DEFAULT NULL,
  `tel_number` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `cre_date` datetime NOT NULL,
  `mod_date` datetime NOT NULL,
  `type_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `chat_status` int(11) NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `member_data`
--
-- User Demo for request this url : http://127.0.0.1:4500/member/chat/:id/:token
-- User1 http://127.0.0.1:4500/member/chat/1/f1e3e704019ee655faa1eb82240b585f
-- User2 http://127.0.0.1:4500/member/chat/10/c58d6aec5156f070dce5b4faefdfab64
--
INSERT INTO `member_data` (`member_id`, `first_name`, `last_name`, `e_mail`, `mem_password`, `tel_number`, `facebook`, `cre_date`, `mod_date`, `type_id`, `token`, `chat_status`) VALUES
(1, 'Sirirat', 'Saemak', 'test@test.com', '81dc9bdb52d04dc20036dbd8313ed055', '89090336', 'test', '2014-10-03 15:14:37', '2014-10-27 19:25:43', 1, 'f1e3e704019ee655faa1eb82240b585f', 0),
(10, 'Woramet', 'Muangsiri', 'test2@test.com', '81dc9bdb52d04dc20036dbd8313ed055', '', '953885277961518', '2014-10-21 12:33:32', '2014-10-28 13:26:15', 2, 'c58d6aec5156f070dce5b4faefdfab64', 0)


-- --------------------------------------------------------

--
-- Table structure for table `venue_data`
--

CREATE TABLE IF NOT EXISTS `venue_data` (
  `venue_id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `venue_name` varchar(255) NOT NULL,
  `venue_major` varchar(255) DEFAULT NULL,
  `majorID` varchar(255) NOT NULL,
  `minorID` varchar(255) DEFAULT NULL,
  `venue_user` varchar(255) DEFAULT NULL,
  `venue_pass` varchar(255) DEFAULT NULL,
  `welcome_image` varchar(255) DEFAULT NULL,
  `welcome_message` varchar(255) DEFAULT NULL,
  `isuniversal` int(11) NOT NULL,
  `noti_status` int(11) NOT NULL,
  `cre_date` datetime NOT NULL,
  `mod_date` datetime NOT NULL,
  `chat_status` int(11) NOT NULL,
  PRIMARY KEY (`venue_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `venue_data`
--
--
-- Username & Password for login : http://127.0.0.1:4500
-- username : venue@test.com password : 1234
--

INSERT INTO `venue_data` (`venue_id`, `token`, `venue_name`, `venue_major`, `majorID`, `minorID`, `venue_user`, `venue_pass`, `welcome_image`, `welcome_message`, `isuniversal`, `noti_status`, `cre_date`, `mod_date`, `chat_status`) VALUES
(3, '1b993f46b47bf32d3537342dbf7d6c59', 'Clouds', 'Center', '12721', '12721', 'venue@test.com', '81dc9bdb52d04dc20036dbd8313ed055', '2e9178cf9bb9eb8e08a1ffa660182c47.png', 'Welcome to Clouds !!', 1, 1, '2014-10-17 14:33:08', '2014-10-24 15:06:29', 1);
